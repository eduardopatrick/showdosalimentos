Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'requisicaoresposta' => "foodgame#requisicaoresposta"
  resources :logerros
  root "principal#index"
  resources :perguntas
  resources :usuarios
  resources :principal
  get 'inserirpergunta' => "perguntas#new"
end
