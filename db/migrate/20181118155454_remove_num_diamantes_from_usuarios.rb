class RemoveNumDiamantesFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :usuarios, :num_diamantes, :string
  end
end
