class CreateErros < ActiveRecord::Migration[5.1]
  def change
    create_table :erros do |t|
      t.string :usuario
      t.string :data
      t.string :versaoAndroid
      t.string :fabricante
      t.string :modelo
      t.string :stackTrace

      t.timestamps
    end
  end
end
