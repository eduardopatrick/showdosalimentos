class AddAlturaFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    add_column :usuarios, :altura, :float
  end
end
