class RemoveAlturaFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :usuarios, :altura, :string
  end
end
