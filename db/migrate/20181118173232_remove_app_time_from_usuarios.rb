class RemoveAppTimeFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :usuarios, :app_time, :string
  end
end
