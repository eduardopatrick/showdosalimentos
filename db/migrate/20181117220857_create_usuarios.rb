class CreateUsuarios < ActiveRecord::Migration[5.1]
  def change
    create_table :usuarios do |t|
      t.string :email
      t.string :senha
      t.string :avatar
      t.string :nome
      t.string :sobrenome
      t.string :genero
      t.string :idade
      t.string :peso
      t.string :altura
      t.string :serie
      t.string :num_moedas
      t.string :num_diamantes
      t.string :app_time
      t.string :partidas
      t.string :imc

      t.timestamps
    end
  end
end
