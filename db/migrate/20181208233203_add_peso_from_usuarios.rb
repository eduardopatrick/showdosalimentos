class AddPesoFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    add_column :usuarios, :peso, :float
  end
end
