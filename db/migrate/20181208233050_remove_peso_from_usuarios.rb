class RemovePesoFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :usuarios, :peso, :string
  end
end
