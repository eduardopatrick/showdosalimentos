class RemoveNumMoedasFromUsuarios < ActiveRecord::Migration[5.1]
  def change
    remove_column :usuarios, :num_moedas, :string
  end
end
