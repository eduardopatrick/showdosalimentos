class CreatePergunta < ActiveRecord::Migration[5.1]
  def change
    create_table :pergunta do |t|
      t.string :pergunta
      t.string :dica
      t.string :alternativa_correta
      t.string :alternativa_2
      t.string :alternativa_3
      t.string :alternativa_4
      t.string :explicacao
      t.string :nivel
      t.string :num_acertos
      t.string :num_erros

      t.timestamps
    end
  end
end
