class CreatePergResps < ActiveRecord::Migration[5.1]
  def change
    create_table :perg_resps do |t|
      t.string :email
      t.text :respondidas

      t.timestamps
    end
  end
end
