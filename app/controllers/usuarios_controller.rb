class UsuariosController < ApplicationController
    before_action :authenticate_user!
    def index
        @usuarios = Usuario.all
    end    

    def destroy 
        @usuario = Usuario.find(params[:id])
        @usuario.destroy
          redirect_to "/usuarios", success: 'Usuario deletado com sucesso.' 
    end 


  helper_method :getimc
end
