class LogerrosController < ApplicationController
    before_action :authenticate_user!
    def index
        @logerros = Erro.all
    end    

    def destroy 
        @erro = Erro.find(params[:id])
        @erro.destroy
         redirect_to "/logerros", success: 'Erro deletado com sucesso.' 
    end    
end
