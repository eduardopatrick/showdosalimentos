class PerguntasController < ApplicationController
  before_action :authenticate_user!
    def index
        @perguntas = Perguntum.all
    end    

    def new
      @pergunta = Perguntum.new
    end 

    def create
        @pergunta = Perguntum.new(pergunta_params)
  
      if @pergunta.save
        redirect_to "/perguntas", success: "Pergunta inserida com sucesso!"
      else
        redirect_to "/perguntas", danger: "Falha ao inserir pergunta!"
      end
    end    

    def destroy 
        @pergunta = Perguntum.find(params[:id])
        @pergunta.destroy
         redirect_to "/perguntas", success: 'Pergunta deletada com sucesso.' 
    end 

    private
    def pergunta_params
      params.require(:perguntum).permit(:pergunta, :dica, :alternativa_correta, :alternativa_2, :alternativa_3, :alternativa_4, :nivel, :explicacao)
    end
end