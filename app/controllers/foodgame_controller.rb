class FoodgameController < ActionController::API

    def requisicaoresposta
      # Exemplo: {"requisicao":"login","email":"josé@gmail.com","senha":"12345"}
      # Exemplo: {"requisicao":"alterar_senha","email":"josé@gmail.com","senha":"12345","novaSenha":"54321"}
     request.body.rewind 
     data = JSON.parse (request.body.read || '{"requisicao":"erro"}')
     requisicao =  data['requisicao']
 
   case requisicao 
            when "login"   
                  login = Login.where(login_params)	 
              if !login.empty?
                      render json: {status: 'SUCCESS', resultado:'loginAceito', usuario:login},status: :ok
              else	
              render json: {status: 'ERROR', resultado:'loginNegado', usuario:login},status: :unprocessable_entity
              end

            when "alterar_senha"   
              novasenha = params[:novaSenha]  
              if !Login.where(email: params[:email], senha: params[:senha]).empty? 
              Login.update(senha: novasenha)	
            render json: {status: 'SUCCESS', resultado:'sucesso', data:requisicao},status: :ok
              else
              render json: {status: 'ERROR', resultado:'erro', data:requisicao},status: :unprocessable_entity
              end

            when "pegar_usuario"  
            usuario = Usuario.find_by_id(params[:id])
              if !usuario.nil?
              render json: {status: 'SUCCESS', resultado:'sucesso', usuario:usuario},status: :ok
                          else
              render json: {status: 'ERROR', resultado:'erro', usuario:usuario},status: :unprocessable_entity 
              end

            when "pegar_perguntas"  
              nivel = params[:nivel]
              useremail = params[:email_usuario]
              perguntas = Perguntum.where("nivel LIKE ?", "#{params[:nivel]}")
              if !perguntas.nil?
              render json: {status: 'SUCCESS', resultado:'sucesso', lista_de_perguntas:perguntas},status: :ok
              else	
              render json: {status: 'ERROR', resultado:'erro'},status: :unprocessable_entity
              end

            when "pegar_pergunta" 
              nivel = params[:nivel]
              useremail = params[:email_usuario]
              pergunta = Perguntum.where("nivel LIKE ?", "#{params[:nivel]}").first  
              if !pergunta.nil?
              render json: {status: 'SUCCESS', resultado:'sucesso', pergunta:pergunta},status: :ok
              else	
              render json: {status: 'ERROR', resultado:'erro', data:requisicao},status: :unprocessable_entity
              end

            when "efetuar_cadastro" 
              login = Login.new(email: params[:email], senha: params[:senha])
              usuario = Usuario.new(usuario_params)
              pergresp = PergResp.new(pergresp_params)
              if login.save && usuario.save && pergresp.save
              render json: {status: 'SUCCESS', resultado:'sucesso', data:requisicao},status: :ok
              else	
              render json: {status: 'ERROR', resultado:'erro', data:requisicao},status: :unprocessable_entity
              end

            when "pega_ranking_geral"  
              usuario = params[:email_usuario]
              usuarios = Usuario.order(num_diamanates: :desc)
              posicao = usuarios.pluck(:email).index(usuario)
              data = ""
                render json: {status: 'SUCCESS', resultado:'sucesso', lista_ranking:usuarios, posicao:posicao},status: :ok
        
            when "pega_ranking_idade"   
             usuario = params[:email_usuario] 
             usuarios = Usuario.where("idade LIKE ?", "#{params[:idade]}").order(num_diamanates: :desc)
             posicao = usuarios.pluck(:email).index(usuario)
             data = ""
              render json: {status: 'SUCCESS', resultado:'sucesso', lista_ranking:usuarios, posicao:posicao},status: :ok
        
            when "pega_ranking_escolaridade"   
            usuario = params[:email_usuario] 
             usuarios = Usuario.where("serie LIKE ?", "#{params[:serie]}").order(num_diamanates: :desc)
             posicao = usuarios.pluck(:email).index(usuario)
             data = ""
               render json: {status: 'SUCCESS', resultado:'sucesso', lista_ranking:usuarios, posicao:posicao},status: :ok    

            when "fim_de_jogo"   
              email = params[:email_usuario]
              moedas = params[:num_moedas]
              diamantes = params[:num_diamantes]
              perguntas = params[:perguntas_corretas]
              usuario = Usuario.where("email LIKE ?", "#{params[:email_usuario]}") 
              PergResp.where("email LIKE ?", "#{params[:email_usuario]}").new(respondidas: perguntas)
              usuario.update(usuario_params)
              render json: {status: 'SUCCESS', resultado:'sucesso'},status: :ok
                        
            when "aumenta_tempo_de_jogo" 
              email = params[:email_usuario]
              tempo = params[:tempo_adicionado]
              usuario = Usuario.where("email LIKE ?", "#{params[:email_usuario]}").first 
              tempo = (usuario[:app_time].to_f) + (tempo.to_f)
              usuario.update(app_time: tempo)
              render json: {status: 'SUCCESS', resultado:'sucesso'},status: :ok


            when "envia_log_de_erro"   
              erro = Erro.new(erro_params)
              if erro.save
                render json: {status: 'SUCCESS', resultado:'sucesso', data:requisicao},status: :ok
              else	
              render json: {status: 'ERROR', resultado:'erro', data:requisicao},status: :unprocessable_entity
              end

      else
             render json: {status: 'ERROR', erro:'Requisição Desconhecida', data:requisicao},status: :unprocessable_entity
       end
  end	
 
        def login_params
         params.permit(:email, :senha, :novaSenha)
        end
 
        def erro_params
          params.permit(:usuario, :data, :versaoAndroid, :fabricante, :modelo, :stackTrace)
        end
   
        def pergunta_params
          params.permit(:pergunta, :dica, :alternativa_correta, :alternativa_2, :alternativa_3, :alternativa_4, :explicacao, :nivel, :num_acertos, :num_erros)
        end
      
        def usuario_params
          params.permit(:id, :email, :senha, :avatar, :nome, :sobrenome, :genero, :idade, :peso, :altura, :serie, :num_moedas, :num_diamanates, :app_time, :partidas)
        end

        def pergresp_params
          params.permit(:email, :respondidas)
        end
 end
 
